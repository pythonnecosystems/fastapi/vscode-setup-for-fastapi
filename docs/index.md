# FastAPI용 VSCode 설정 <sup>[1](#footnote_1)</sup>

저자는 개인적으로 많은 개발자들에게 사실상의 Python용 IDE인 것처럼 보이는 PyCharm보다 VSCode를 훨씬 더 좋아한다. 그 이유 중 하나는 Python에 대한 합리적인 기본값으로 작업을 쉽게 시작할 수 있다는 점이다. 이는 매우 좋지만 많은 플러그인이 있는 유연한 UI를 포기하는 것이며, 간단한 구성으로 PyCharm만큼 좋고 그 이상도 될 수 있다!

<a name="footnote_1">1</a>: 이 페이지는 [VSCode setup for FastAPI](https://snir-orlanczyk.medium.com/fastapi-project-in-vscode-7fa3e6fccdb2)를 편역하였다.
